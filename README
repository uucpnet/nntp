NNTP README		December 3, 1994  (NNTP 1.5.12)

[See the file CHANGES to see differences between this version and
older versions.]

INTRODUCTION

     This package contains the code necessary to compile an NNTP server
daemon.  If you need the NNTP client code, get the 1.6 release of the
client software.

     A brief tour of the directories and their programs:

	.		The "conf.h" file needs to be edited to reflect
			the peculiarities of your system.

	server		Source for the NNTP news server daemon.

	support		Some support files and scripts.

	doc		Documentation on the server, including manual
			pages.

	xmit		An active transmission client for transferring
			news, written by Erik Fair; see note below.

	xfer		A passive reception client which uses the
			NEWNEWS command to retrieve news from a remote
			server.  Written by Brian Kantor, this software
			works more or less. Bug reports are welcome.

     Each directory has associated with it a README file.
This file will try to give enough information for you to get things
running and installed, but the README in each directory has more
details for each subset of the NNTP package.  You may also want to print
a copy of doc/rfc977, which describes the NNTP protocol.

INTRODUCTION TO NNTP INSTALLATION

     First, figure out what you are trying to do (this is good
advice under most circumstances, and it is especially apropos here).
NNTP can be used for two things:  (1) Remote news reading, where news
is stored on one machine and read from remote machines across a
high-speed local area network such as Ethernet, and (2) News transfer,
where news is transmitted to NNTP servers on remote machines over
either local or long-haul networks.

     NNTP "server" machines are machines that have a full installation
of USENET news on them.  An NNTP process, the "server", allows remote
sites to connect to the server machine and read or transfer news.
The server machine DOES NOT NEED "reader client" software such as
"rrn".  It MAY NEED "transmission client" software such as "nntpxmit"
if you want to use NNTP to transfer news.

     NNTP "client" machines do not have a full installation of USENET
news on them.  They get their news from an NNTP server machine across
the network.  They DO have NNTP "reader clients" such as "rrn" installed
on them.

     In summary,

	>>> A full client installation of NNTP may require the following
	    files (suitable for rdist, assuming standard directories):

NEWS = ( /usr/local/bin/{Pnews,Rnmail,inews,rn,rrn,newsetup,newsgroups}
	/usr/local/lib/rn 
	/usr/man/catl/{Pnews,Rnmail,rn,newsetup,newsgroups}.1 )

	    You DO NOT need any of the normal news junk (e.g.,
	    /usr/lib/news, /usr/spool/news, /usr/spool/batch) on CLIENT
	    systems.

	    You DO need these on SERVER systems.

     An important note:

	The NNTP server assumes that the history file format
	is 2.11.19 BNEWS or CNEWS; therefore you need 2.11.19 BNEWS or CNEWS
	on your	server machine.

	>>>>> Get 2.11.19 BNEWS or CNEWS if you don't have it.

GENERAL INSTALLATION

     Time for a general and cohesive Plan:

You will have to edit conf.h to reflect your system's setup.  So,

     1. Look at README-conf.h.  This will explain the stuff
	needs to be tailored for your system in conf.h.
	Make the necessary changes to reflect your system.

     2. Type "make" in this directory.

     3. Type "make install" in this directory.

IF YOU HAVE PROBLEMS

     Contact Stan Barber <nntp@academ.com> for nntp bugs/questions/etc.

     I'm very interested in learning what hacks need to be made to
nntpd to get it to work on various systems, and certainly, if there are
outright bugs, please let me know.  Bug reports and fixes for nntp are
posted to the newsgroup "news.software.nntp".  Announcements of new
versions of nntp software are posted there, too.
